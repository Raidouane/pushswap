##
## Makefile for Makefile in /home/el-mou_r/rendu/push_swap/CPE_year_Pushswap
## 
## Made by Raidouane EL MOUKHTARI
## Login   <el-mou_r@epitech.net>
## 
## Started on  Mon Nov  2 16:00:35 2015 Raidouane EL MOUKHTARI
## Last update Thu Nov 19 23:57:02 2015 Raidouane EL MOUKHTARI
##

CC	= gcc -g

RM	= rm

NAME	= push_swap

FLAG	= -Iinclude -Wall -W

SRC	=src/push_swap.c		\
	src/pushswap.c			\
	src/push_swap_operation.c	\
	src/my_putstr.c			\
        src/my_getnbr.c			\
	src/my_strlen.c			\
	src/my_putchar.c

OBJ	= $(SRC:.c=.o)

$(NAME):	$(OBJ)
	$(CC) $(OBJ) $(FLAG) -o $(NAME)

all:	$(NAME)

clean:
	$(RM) -f $(OBJ)

fclean:	clean
	$(RM) -f $(NAME)

re:	fclean all
