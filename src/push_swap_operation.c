/*
** push_swap_operation.c for push_swap_operation.c in /home/el-mou_r/rendu/push_swap/CPE_year_Pushswap
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Nov 16 14:44:00 2015 Raidouane EL MOUKHTARI
** Last update Sat Nov 21 00:33:29 2015 Raidouane EL MOUKHTARI
*/

#include "../include/my.h"
#include "../include/mylist.h"
#include <stdlib.h>

void		pa_function(t_struct *ps, t_list **la, int stop_space)
{
  t_list	*tmp;
  t_list	*tmpa;
  t_list	*end;

  tmp = ps->lb;
  ps->lb = ps->lb->next;
  tmp->next = *la;
  *la = tmp;
  my_putstr("pa");
  if (stop_space == 0)
    my_putchar(' ');
  else if (stop_space == 1)
    my_putchar('\n');
}

void		pb_function(t_struct *ps, t_list **la)
{
  t_list	*tmp;
  t_list	*end;

  tmp = *la;
  end = (*la)->prev;
  *la = (*la)->next;
  (*la)->prev = end;
  end->next = (*la);
  tmp->next = ps->lb;
  ps->lb = tmp;
  my_putstr("pb ");
}

void		ra_function(t_struct *ps, t_list **la)
{
  t_list	*first;

  first = *la;
  *la = (*la)->next;
  (*la)->prev = first;
  my_putstr("ra ");
}

void		rra_function(t_struct *ps, t_list **la)
{
  (*la) = (*la)->prev;
  my_putstr("rra ");
}
