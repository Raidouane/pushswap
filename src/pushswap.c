/*
** pushswap.c for pushswap.c in /home/el-mou_r/rendu/push_swap/CPE_year_Pushswap
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Nov 17 15:38:42 2015 Raidouane EL MOUKHTARI
** Last update Sun Nov 22 15:53:01 2015 Raidouane EL MOUKHTARI
*/

#include <stdlib.h>
#include "../include/my.h"
#include "../include/mylist.h"

void		check_min(t_struct *ps, t_list ***la)
{
  t_list	*tmp;

  tmp = (**la)->next;
  ps->stk_a = (**la)->val;
  ps->end = (**la)->prev;
  while (tmp->next != ps->end->next && tmp != ps->end->next)
    {
      if (ps->stk_a > tmp->val)
	{
	  ps->stk_a = tmp->val;
	  tmp = tmp->next;
	}
      else
	tmp = tmp->next;
    }
  if (ps->stk_a > tmp->val)
    ps->stk_a = tmp->val;
}

void	execute_rotations(t_struct *ps, t_list ***la, int inc)
{
  int	save_inc;
  int	i;

  i = ps->save_i;
  i = i / 2;
  save_inc = inc;
  while (inc > 0 && save_inc <= i)
    {
      ra_function(ps, *la);
      inc--;
    }
  save_inc = 0;
  while (inc > i && save_inc < (ps->save_i - inc))
    {
      rra_function(ps, *la);
      save_inc++;
    }
}

int		find_pos_min(t_struct *ps, t_list ***la)
{
  int		inc;
  t_list	*tmpb;

  inc = 0;
  tmpb = (**la);
  while (tmpb->val != ps->stk_a && tmpb->next != ps->end->next)
    {
      inc++;
      tmpb = tmpb->next;
    }
  if (ps->stk_a == (**la)->val)
    inc = 0;
  return (inc);
}

int		check_already_ifsorted(t_struct *ps, t_list ***la, int nb_arg)
{
  t_list	*tmp;
  int		pg;
  int		i;
  int		c;

  c = 0;
  pg = (**la)->val;
  i = 0;
  tmp = (**la)->next;
  while (tmp != **la)
    {
      if (pg <= tmp->val)
	{
	  pg = tmp->val;
	  i++;
	}
      tmp = tmp->next;
      c++;
    }
  if (i == (nb_arg - 1))
    {
      my_putchar('\n');
      exit(1);
    }
}

int		function_manage(t_struct *ps, t_list **la, int i)
{
  int		inc;

  i = i - 1;
  ps->stk_i = i;
  ps->stk_a = (*la)->next->val;
  check_already_ifsorted(ps, &la, i);
  while (*la && i > 1)
    {
      if (i == 1)
	{
	  pb_function(ps, la);
	  i = 0;
	}
      else if (i > 1)
	{
	  check_min(ps, &la);
	  inc = find_pos_min(ps, &la);
	  ps->save_i = i;
	  execute_rotations(ps, &la, inc);
	  pb_function(ps, la);
	  i = ps->save_i;
	  i--;
	}
    }
  filling_list_sorted(ps, &la);
}
